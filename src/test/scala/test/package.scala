import scala.collection.mutable.ArrayBuffer

import app.Customer

package object mocks {
  val timeArr = Array[Int](1, 3)
  val customer = Customer(timeArr.head, timeArr.last)
  val customers1 = ArrayBuffer(Customer(0, 3), Customer(1, 9), Customer(2, 5))
  val customers2 = ArrayBuffer(Customer(0, 3), Customer(1, 9), Customer(2, 6))
}
