package test

import app.Calculation._
import app.Error.CorruptedDataError
import org.scalatest.{FlatSpec, Matchers}

class CalculationTest extends FlatSpec with Matchers {
 
  "extractTime" should "return Right(Array[Int]) with two elements from data" in {
    extractTime("1  2").right.get shouldBe Array(1, 2)
  }
  
  it should "return Left(CorruptedDataError) in case corrupted data" in {
    //one space between numbers is unacceptable
    extractTime("1 2").left.get shouldBe CorruptedDataError
    extractTime("word").left.get shouldBe CorruptedDataError
    extractTime("word word").left.get shouldBe CorruptedDataError
    extractTime(" ").left.get shouldBe CorruptedDataError
  }
  
  "minAvrWaitTime" should "return 8" in {
    minAvrWaitTime(mocks.customers1.sortBy(x => (x.l, x.t))) shouldBe 8
  }
  
  it should "return 9" in {
    minAvrWaitTime(mocks.customers2.sortBy(x => (x.l, x.t))) shouldBe 9
  }
  
}
