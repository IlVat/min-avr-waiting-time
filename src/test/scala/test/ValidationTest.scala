package test

import app.Error.{NumberBoundError, NumberFormatError}
import org.scalatest.{FlatSpec, Matchers}
import app.Validation._

class ValidationTest extends FlatSpec with Matchers {
  "validate" should "return Right(3)" in {
    validate("3", 1, 100) shouldBe Right(3)
  }
  
  it should "return Left(NumberFormatError)" in {
    validate("error", 1, 100) shouldBe Left(NumberFormatError)
  }
  
  it should "return Left(NumberBoundError)" in {
    validate("3", 4, 100) shouldBe Left(NumberBoundError)
    validate("3", 1, 2) shouldBe Left(NumberBoundError)
  }
}
