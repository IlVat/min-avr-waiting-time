package app


case class Customer(t: Int, l: Int)

sealed trait Error

object Error {
  case object NumberFormatError extends Error
  case object NumberBoundError extends Error
  case object CorruptedDataError extends Error
}
