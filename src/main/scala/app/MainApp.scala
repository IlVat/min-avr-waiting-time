package app

import scala.collection.mutable.ArrayBuffer

import app.Calculation._
import app.Validation._

object MainApp extends App {
  val stdin = io.StdIn
  
  println("Acceptable data boundaries:")
  println()

  println(s" - customers number (lower boundary): $customersLowerBound")
  println(s" - customers number (upper boundary): $customersUpperBound")
  println()

  println(s" - waiting time (lower boundary): $timeLowerBound")
  println(s" - waiting time (upper boundary): $timeUpperBound")
  println()

  println("Enter number of customers:")

  while (customersNumber == 0) {
    for {
      validNum <- validate(stdin.readLine(), customersLowerBound, customersUpperBound)
      customers <- {
        customersNumber = validNum
        println("Enter customers t and l:")
        fillCustomersArray(ArrayBuffer.empty[Customer], customersNumber, stdin)
      }
      _ <- Right {
        val sortedCustomers = customers.sortBy(c => (c.l, c.t))
        println(s"Minimal average waiting time: ${ minAvrWaitTime(sortedCustomers) }")
      }
    } yield ()
  }
}



