package app

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.io.StdIn

import app.Error.CorruptedDataError
import app.Validation.validate

object Calculation {
  val customersLowerBound: Int = 1
  val customersUpperBound: Int = 100000
  val timeLowerBound: Int = 0
  val timeUpperBound: Int = 1000000000
  val regex = """^\d+\s{2}\d+$"""
  var customersNumber = 0
  val customers = ArrayBuffer.empty[Customer]
  
  def fillCustomersArray(
    customers: mutable.Buffer[Customer],
    customerNumber: Int,
    input: StdIn.type): Either[Unit, mutable.Buffer[Customer]] = {
    while (customers.size < customerNumber) {
      val line = input.readLine
      if (line.matches(regex)) {
        extractTime(line) match {
          case Right(timeArr) => Right(customers += Customer(timeArr.head, timeArr.last))
          case _ => Left(())
        }
      }
      else {
        println("Wrong input format. Enter two integer numbers that is separated by two spaces")
        Left(())
      }
    }
    Right(customers)
  }
  
  def extractTime(data: String): Either[Error, Array[Int]] = {
    val arr: Array[Int] = data.split("  ").flatMap { str =>
      validate(str.trim, timeLowerBound, timeUpperBound) match {
        case Right(num) => Array(num)
        case _ => Array.empty[Int]
      }
    }
    
    if (arr.length == 2) Right(arr)
    else Left(CorruptedDataError)
  }
  
  def minAvrWaitTime(customers: mutable.Buffer[Customer]): Long = {
    val fullWaitTime = customers.foldLeft((0L, 0L)) {
      case ((pizzaCookTimeAcc, waitTimeAcc), customer) =>
        (pizzaCookTimeAcc + customer.l, calcWaitTimeAcc(waitTimeAcc, pizzaCookTimeAcc, customer))
    }
    fullWaitTime._2 / customers.size
  }
  
  def calcWaitTimeAcc(waitTimeAcc: Long, pizzaCookTimeAcc: Long, customer: Customer): Long = {
    if (waitTimeAcc > customer.t) {
      val currentWaitTime = pizzaCookTimeAcc + (customer.l - customer.t)
      waitTimeAcc + currentWaitTime
    }
    else waitTimeAcc + customer.l
  }
}
