package app

import scala.util.{Success, Try}

import app.Error.{NumberBoundError, NumberFormatError}

object Validation {
  def validate(str: String, lowerBound: Int, upperBound: Int): Either[Error, Int] = for {
    intNum <- str2Int(str)
    checkedNum <- checkBounds(intNum, lowerBound, upperBound)
  } yield checkedNum
  
  def str2Int(str: String): Either[Error, Int] = {
    Try(str.toInt) match {
      case Success(num) => Right(num)
      case _ =>
        println(s"The input data = $str must be integer!")
        Left(NumberFormatError)
    }
  }
  
  def checkBounds(number: Int, lowerBound: Int, upperBound: Int): Either[Error, Int] = {
    if (lowerBound <= number && number <= upperBound) Right(number)
    else {
      println(s"Exceeded acceptable boundaries in $number")
      Left(NumberBoundError)
    }
  }
}
